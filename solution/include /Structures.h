#pragma once

#include <stdint.h>
#include <stdio.h>
#pragma pack(push, 1)

typedef struct Header{
    unsigned char BM[2];
    uint32_t biSize;
    uint32_t Unused;
    uint32_t biOffset;
    uint32_t sizeHeader;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bits;
    uint32_t compression;
    uint32_t sizeImage;
    uint32_t xPerMeter;
    uint32_t yPerMeter;
    uint32_t clrUsed;
    uint32_t indColor;
} Header;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct Color{
    uint8_t b, g, r;
} Color;
#pragma pack(pop)

static const size_t size_8 = sizeof(uint8_t);
static const size_t size_16 = sizeof(uint16_t);
static const size_t size_32 = sizeof(uint32_t);
static const size_t size_color = sizeof(Color);


typedef enum Status{
    OK = 0,
    BM = 1,
    BITS = 2,
    ERROR_SIZE = 3,
    COMP = 4
} Status;


typedef struct Image{
    uint32_t width, height;
    Color** colors;
} Image;




