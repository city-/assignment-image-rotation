#include "...include/Files.h"
#include "...include/Rotation.h"
#include <malloc.h>
#include <stdio.h>



char* Errors[] = {"Success read","Error bm bytes", "Error count bits", "Error size", "Error compression" };

int main(int argc, char* argv[]) {
    if(argc >= 3){
        FILE* Source = fopen(argv[1], "r");
        FILE* New = fopen(argv[2], "w");

        if(Source == NULL || New == NULL) {
            printf("file not found");
            return -1;
        }

        Image* image = malloc(sizeof(Image));
        printf("%s\n", Errors[loadBmp(Source, image)]);

        Image* new = rotate(image);

        createBMP(New, new);

        fclose(Source);
        fclose(New);

        for (int i = 0; i < image->height; ++i) {
            free(image->colors[i]);
        }
        free(image->colors);
        free(image);
        for (int i = 0; i < new->height; ++i) {
            free(new->colors[i]);
        }
        free(new->colors);
        free(new);

        return 0;
    }
    else{
        printf("count args not 3 and more");
        return -4;
    }

}

