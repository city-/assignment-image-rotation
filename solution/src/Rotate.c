#include "../include/Rotation.h"
#include "../include/Structures.h"
#include <malloc.h>



Image* rotate(Image* const src){
    long long width = src->width;
    long long height = src->height;

    Image* new = malloc(sizeof(Image));
    new->width = height;
    new->height = width;

    new->colors = malloc(sizeof(Color*) * width);

    for (int i = 0; i < width; ++i) {
        new->colors[i] = malloc(sizeof(Color) * height);
    }

    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            new->colors[x][ y] = src->colors[height - y - 1][x];
        }
    }

    return new;
}
