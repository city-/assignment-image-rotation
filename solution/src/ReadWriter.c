#include "../include/Files.h"
#include "../include/Structures.h"
#include <malloc.h>
#include <stdio.h>



int zero_pointer = 0;
Header header = {"BM", 0, 0,  54, 40, 0, 0, 1, 24, 0, 0, 0, 0, 0, 0};

Status loadBmp(FILE* Source, Image* New){
    Header headers;

    fseek(Source, 0L, SEEK_END);
    size_t size = ftell(Source);
    rewind(Source);

    fread(&headers, sizeof(Header), 1, Source);

    if(headers.biOffset != header.biOffset || headers.sizeHeader != header.sizeHeader || headers.biSize != size)
        return ERROR_SIZE;
    else if(headers.BM[0] != 'B' || headers.BM[1] != 'M')
        return BM;
    else if(headers.bits != header.bits)
        return BITS;
    else if(headers.compression != header.compression)
        return COMP;
    

    New->width = headers.width;
    New->height = headers.height;
    New->colors = malloc(sizeof(Color*) * headers.height+1);
    
    
    uint32_t padding = (4- ((3 * headers.width) % 4)) % 4;
    for (int i = 0; i < headers.height; ++i) {
        New->colors[i] = (Color*)malloc(size_color * headers.width+1);
        for (int j = 0; j < headers.width; ++j) {
            fread(&New->colors[i][j], size_color, 1, Source);
        }
        fseek(Source, padding, SEEK_CUR);
    }
    return OK;
}


void createBMP(FILE* file, Image* image) {
    unsigned long padding = (4 - ((3 * image->width) % 4)) % 4;
    uint32_t size = 54 + image->width * image->height * 3 + image->height * padding;
    fwrite(&header.BM, size_8 * 2, 1, file);
    fwrite(&size, size_32, 1, file);
    fwrite(&header.Unused, size_32, 1, file);
    fwrite(&header.biOffset, size_32, 1, file);
    fwrite(&header.sizeHeader, size_32, 1, file);
    fwrite(&image->width, size_32, 1, file);
    fwrite(&image->height, size_32, 1, file);
    fwrite(&header.planes, size_16, 1, file);
    fwrite(&header.bits, size_16, 1, file);
    fwrite(&header.compression, size_32, 1, file);
    size -= 54;
    fwrite(&header.sizeImage, size_32, 1, file);
    fwrite(&header.xPerMeter, size_32, 1, file);
    fwrite(&header.yPerMeter, size_32, 1, file);
    fwrite(&header.clrUsed, size_32, 1, file);
    fwrite(&header.indColor, size_32, 1, file);



    for (int x = 0; x < image->height; ++x) {
        for (int y = 0; y < image->width; ++y) {
            fwrite(&image->colors[x][y], size_color, 1, file);
        }
        fwrite(&zero_pointer, padding, 1, file);
    }
}
